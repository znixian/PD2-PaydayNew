
local expm = "lib/managers/experience_manager"

local exp_base = 15000 -- Roughly the same as playing Bank Heist: Cash with a 1x difficulty multiplier

local difficulty_multipliers = {}
local difficulty_curve = 2 -- 1,2,4,8, etc
local difficulty_start = 3 -- difficulty ID for what difficulty should get 1x exp

for diff=1,8 do
	local exponent = diff - difficulty_start
	difficulty_multipliers[diff] = math.pow(difficulty_curve, exponent)
end

OnLoad("lib/tweak_data/tweakdata", function()
	local self = tweak_data
	
	-- Now set up the level system
	self.experience_manager.levels = {}
	
	local levels = 100 -- TODO maybe expand this in the future?
	
	for i=1,levels do
		local diff_id = math.floor(i * #difficulty_multipliers / levels) + 1
		
		-- Cap it off
		if diff_id > #difficulty_multipliers then diff_id = #difficulty_multipliers end
		
		local exp = difficulty_multipliers[diff_id] * exp_base
		
		-- print("EXP_REQ", i, diff_id, exp)
		
		self.experience_manager.levels[i] = {
			points = exp
		}
	end
end)

Hook(expm, "ExperienceManager:get_contract_difficulty_multiplier", function(orig, self, stars)
	return difficulty_multipliers[stars] - 1
end)
