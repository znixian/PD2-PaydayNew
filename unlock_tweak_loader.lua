-- Change the unlock levels
Hook("lib/tweak_data/upgradestweakdata", "UpgradesTweakData:init", function(orig, self, ...)
	orig(self, ...)
	
	local level_mappings = {}
	
	for name, data in pairs(weapon_tweak_data.all) do
		local level = data.level
		
		if level ~= nil then
			level_mappings[name] = level
		end
	end
	
	-- Generate the 'not implemented' warnings
	for name, def in pairs(self.definitions) do
		if not weapon_tweak_data.all[name] and def.category == "weapon" then
			print("UNIMPLEMENTED WEAPON: " .. name)
			level_mappings[name] = 1000 -- Always locked
		end
	end
	
	-- Remove everything with a set level
	for level, data in pairs(self.level_tree) do
		local upgrades = data.upgrades
		
		if upgrades ~= nil then
			local removal_values = {}
			
			for id, item_name in pairs(upgrades) do
				local target_level = level_mappings[item_name]
				if target_level ~= nil and target_level ~= level then
					table.insert(removal_values, id)
				end
			end
			
			-- Always remove the upper values first, so it doesn't mess with the lower indexes.
			table.sort(removal_values, function(a,b) return a>b end)
			
			for _, id in pairs(removal_values) do
				table.remove(upgrades, id)
			end
		end
	end
	
	-- Add the levels for everything back in
	for name, level in pairs(level_mappings) do
		local tree = self.level_tree[level]
		
		if not tree then
			tree = { name_id = "weapons" }
			self.level_tree[level] = tree
		end
		
		if not tree.upgrades then tree.upgrades = {} end
		
		table.insert(tree.upgrades, name)
	end
	
	-- Generate the 'does not exist' errors
	for name, _ in pairs(weapon_tweak_data.all) do
		local def = self.definitions[name]
		if not def or def.category ~= "weapon" then
			error("[PaydayNew] WEAPON DOES NOT EXIST: " .. name)
		end
	end
end)

