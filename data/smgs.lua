
weapon_tweak_data.smgs = {
	
	mp7 = {
		level = 5
	},
	
	mac10 = {
		level = 10
	},
	
	m45 = {
		level = 30
	},
	
	new_mp5 = {
		level = 33
	},
	
	hajk = {
		level = 35
	},
	
	sr2 = {
		level = 38
	},
	
	sterling = {
		level = 40
	},
	
	olympic = {
		level = 43
	},
	
	cobray = {
		level = 45
	},
	
	baka = {
		level = 45
	},
	
	mp9 = {
		level = 48
	},
	
	m1928 = {
		level = 50
	},
	
	scorpion = {
		level = 53
	},
	
	schakal = {
		level = 55
	},
	
	p90 = {
		level = 57
	},
	
	tec9 = {
		level = 60
	},
	
	uzi = {
		level = 65
	},
	
	polymer = {
		level = 70
	},
	
	coal = {
		level = 75
	},
	
	akmsu = {
		level = 80
	},
	
-- 	aaaaaaaaaaaaaaaa = {
-- 		level = aaaaaaaaaaaaaaaaaaaaaaaa
-- 	},
	
}
