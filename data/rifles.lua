
weapon_tweak_data.rifles = {
	
	-- Amcar
	amcar = {
		level = 0
	},
	
	-- Lion's Roar
	vhs = {
		level = 5
	},
	
	-- AK-5
	ak5 = {
		level = 5
	},
	
	-- AMR-16
	m16 = {
		level = 10
	},
	
	-- UAR
	aug = {
		level = 15
	},
	
	-- JP-36
	g36 = {
		level = 20
	},
	
	-- AKM/AK 7.62
	akm = {
		level = 25
	},
	
	-- Golden AKM/AK 7.62
	akm_gold = {
		level = 25
	},
	
	-- Commando 553
	s552 = {
		level = 30
	},
	
	-- Eagle Heavy
	scar = {
		level = 35
	},
	
	-- My Little Friend
	contraband = {
		level = 40
	},
	
	-- M308
	new_m14 = {
		level = 45
	},
	
	-- Gecko 7.62
	galil = {
		level = 50
	},
	
	-- Gewehr 3
	g3 = {
		level = 55
	},
	
	-- Queen’s Wrath
	l85a2 = {
		level = 60
	},
	
	-- CAR-4
	new_m4 = {
		level = 63
	},
	
	-- Falcon
	fal = {
		level = 65
	},
	
	-- Clarion
	famas = {
		level = 68
	},
	
	-- Bootleg
	tecci = {
		level = 70
	},
	
	-- AK12 / AK17
	flint = {
		level = 73
	},
	
	-- AS Val / Valkyria
	asval = {
		level = 75
	},
	
	-- Cavity 9mm
	sub2000 = {
		level = 77
	},
	
	-- AK-74
	ak74 = {
		level = 80
	},
}
