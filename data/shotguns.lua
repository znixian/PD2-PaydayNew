
weapon_tweak_data.shotguns = {
	
-- Primaries
	
	-- Preditor 12G
	spas12 = {
		level = 10
	},
	
	-- M1014
	benelli = {
		level = 15
	},
	
	-- Mosconi 12G
	huntsman = {
		level = 20
	},
	
	-- Raven
	ksg = {
		level = 25
	},
	
	-- Breaker 12G
	boot = {
		level = 30
	},
	
	-- Reinfeld 880
	r870 = {
		level = 35
	},
	
	-- Joceline O/U 12G
	b682 = {
		level = 40
	},
	
	-- Steakout
	aa12 = {
		level = 45
	},
	
	-- IZHMA 12G
	saiga = {
		level = 50
	},
	
-- Secondaries
	
	-- Goliath 12G
	rota = {
		level = 30
	},
	
	-- Street sweeper
	striker = {
		level = 40
	},
	
	-- GSPS 12G
	m37 = {
		level = 50
	},
	
	-- Locomotive 12G
	serbu = {
		level = 60
	},
	
	-- The Judge
	judge = {
		level = 70
	},
	
}
