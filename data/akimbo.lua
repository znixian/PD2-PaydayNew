
weapon_tweak_data.akimbo = {
	
---- Pistols
	
	-- Akimbo Intercepter 45
	x_usp = {
		level = 30
	},
	
	-- Akimbo Chimano 88
	x_g17 = {
		level = 35
	},
	
	-- Akimbo Crosskill
	x_1911 = {
		level = 40
	},
	
	-- Akimbo Chimano Custom
	x_g22c = {
		level = 50
	},
	
	-- Akimbo Bernetti 9
	x_b92fs = {
		level = 55
	},
	
	-- Akimbo Chimano Compact
	jowi = {
		level = 65
	},
	
	-- Akimbo Desert Eagle
	x_deagle = {
		level = 75
	},
	
---- SMGs
	
	-- Akimbo Heather SMGs
	x_sr2 = {
		level = 60
	},
	
	-- Akimbo Compact-5
	x_mp5 = {
		level = 65
	},
	
	-- Akimbo Krinkov
	x_akmsu = {
		level = 80
	},
	
-- 	-- aaaaaaaaaaaaaaaaaaaaaaaaa
-- 	aaaaaaaaaaaaaaaa = {
-- 		level = aaaaaaaaaaaaaaaaaaaaaaaa
-- 	},
}

