
weapon_tweak_data.specials = {
	
-- Primaries
	
	-- Flamethrower
	flamethrower_mk2 = {
		level = 30
	},
	
	-- Vulkan Minigun
	m134 = {
		level = 60
	},
	
	-- GL40
	gre_m79 = {
		level = 30
	},
	
	-- Piglet
	m32 = {
		level = 60
	},
	
	-- Plainsrider bow
	plainsrider = {
		level = 20
	},
	
	-- Light Crossbow
	frankish = {
		level = 25
	},
	
	-- Heavy Crossbow
	arblast = {
		level = 30
	},
	
	-- English Longbow
	long = {
		level = 50
	},
	
-- Secondaries
	
	-- Pistol Crossbow
	hunter = {
		level = 52
	},
	
	-- HRL-7
	rpg7 = {
		level = 30
	},
	
	-- Commando 101
	ray = {
		level = 80
	},
	
	-- China Puff
	china = {
		level = 49
	},
	
	-- Arbiter
	arbiter = {
		level = 70
	},
	
-- 	-- aaaaaaaaaaaaaaaaaaaaaaaaa
-- 	aaaaaaaaaaaaaaaa = {
-- 		level = aaaaaaaaaaaaaaaaaaaaaaaa
-- 	},
	
}
