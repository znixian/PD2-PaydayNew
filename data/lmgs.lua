
weapon_tweak_data.lmgs = {
	
	-- KSP 58
	par = {
		level = 30
	},
	
	-- Brenner 21
	hk21 = {
		level = 30
	},
	
	-- Buzzsaw 42
	mg42 = {
		level = 40
	},
	
	-- RPK
	rpk = {
		level = 45
	},
	
	-- KSP
	m249 = {
		level = 50
	},
	
}
