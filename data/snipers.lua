
weapon_tweak_data.snipers = {
	
	-- Rattlesnake
	msr = {
		level = 10
	},
	
	-- Repeater 1874
	winchester1874 = {
		level = 30
	},
	
	-- Lebensauger .308
	wa2000 = {
		level = 50
	},
	
	-- Desertfox
	desertfox = {
		level = 55
	},
	
	-- Platapus 70
	model70 = {
		level = 60
	},
	
	-- Nagant
	mosin = {
		level = 65
	},
	
	-- R93
	r93 = {
		level = 70
	},
	
	-- Contractor .308
	tti = {
		level = 73
	},
	
	siltstone = {
		level = 75
	},
	
	-- Thanatos
	m95 = {
		level = 80
	},
	
-- 	-- aaaaaaaaaaaaaaaaaaaaaaaaa
-- 	aaaaaaaaaaaaaaaa = {
-- 		level = aaaaaaaaaaaaaaaaaaaaaaaa
-- 	},
	
}
