
weapon_tweak_data.pistols = {
	
	-- Chimano 88
	glock_17 = {
		level = 0
	},
	
	-- Gruber Kurz
	ppk = {
		level = 5
	},
	
	-- White Streak
	pl14 = {
		level = 10
	},
	
	-- LEO
	hs2000 = {
		level = 15
	},
	
	-- Intercepter .45
	usp = {
		level = 20
	},
	
	-- Baby Deagle
	sparrow = {
		level = 25
	},
	
	-- STRYK 18c
	glock_18c = {
		level = 30
	},
	
	-- Matever .357
	mateba = {
		level = 35
	},
	
	-- Crosskill
	colt_1911 = {
		level = 40
	},
	
	-- Peacemaker .45
	peacemaker = {
		level = 45
	},
	
	-- Chimano Custom
	g22c = {
		level = 50
	},
	
	-- Bernetti 9
	b92fs = {
		level = 53
	},
	
	-- Contractor
	packrat = {
		level = 57
	},
	
	-- Signature .40
	p226 = {
		level = 60
	},
	
	-- Chimano Compact
	g26 = {
		level = 65
	},
	
	-- Broomstick
	c96 = {
		level = 70
	},
	
	-- Deagle
	deagle = {
		level = 75
	},
	
	-- Bronco .44
	new_raging_bull = {
		level = 80
	},
	
-- 	-- aaaaaaaaaaaaaaaaaaaaaaaaa
-- 	aaaaaaaaaaaaaaaa = {
-- 		level = aaaaaaaaaaaaaaaaaaaaaaaa
-- 	},
	
}
