
weapon_tweak_data.unaffected = {
	
	-- these are unlocked through skills
	saw = {},
	saw_secondary = {},
	
	-- these do not exist in-game
	contraband_m203 = {},
	
	-- Nonexistant akimbo varients
	x_packrat = {},
	
	-- Non-akimbo primary versions of secondary weapons (these don't exist)
	olympic_primary = {},
	deagle_primary = {},
	akmsu_primary = {},
	glock_18c_primary = {},
	raging_bull_primary = {},
	colt_1911_primary = {},
	b92fs_primary = {},
	
	-- Secondary versions of weapons (these don't exist)
	aug_secondary = {},
	ak74_secondary = {},
	s552_secondary = {},
	m4_secondary = {}
}
