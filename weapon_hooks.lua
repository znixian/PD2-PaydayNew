local function reload_speed_edit(orig, self, ...)
	local mult = orig(self, ...)
	
	if self:weapon_tweak_data().reload_speed_multiplier then
		mult = mult * self:weapon_tweak_data().reload_speed_multiplier
	end
	
	return mult
end

Hook("lib/units/weapons/raycastweaponbase", "RaycastWeaponBase:reload_speed_multiplier", reload_speed_edit)

Hook("lib/units/weapons/newraycastweaponbase", "NewRaycastWeaponBase:reload_speed_multiplier", function(orig, self, ...)
	if self._current_reload_speed_multiplier then
		return self._current_reload_speed_multiplier
	end
	
	return reload_speed_edit(orig, self, ...)
end)

-- Make picking up ammo work with negative minimum bounds
-- TODO find better solution
Hook("lib/units/weapons/raycastweaponbase", "RaycastWeaponBase:add_ammo", function(orig, self, ratio, add_amount_override)
	local _add_ammo = function(ammo_base, ratio, add_amount_override)
		if ammo_base:get_ammo_max() == ammo_base:get_ammo_total() then
			return false, 0
		end
		local multiplier_min = 1
		local multiplier_max = 1
		if ammo_base._ammo_data and ammo_base._ammo_data.ammo_pickup_min_mul then
			multiplier_min = ammo_base._ammo_data.ammo_pickup_min_mul
		else
			multiplier_min = managers.player:upgrade_value("player", "pick_up_ammo_multiplier", 1)
			multiplier_min = multiplier_min + (managers.player:upgrade_value("player", "pick_up_ammo_multiplier_2", 1) - 1)
		end
		if ammo_base._ammo_data and ammo_base._ammo_data.ammo_pickup_max_mul then
			multiplier_max = ammo_base._ammo_data.ammo_pickup_max_mul
		else
			multiplier_max = managers.player:upgrade_value("player", "pick_up_ammo_multiplier", 1)
			multiplier_max = multiplier_max + (managers.player:upgrade_value("player", "pick_up_ammo_multiplier_2", 1) - 1)
		end
		local add_amount = add_amount_override
		local picked_up = true
		if not add_amount then
			local rng_ammo = math.lerp(ammo_base._ammo_pickup[1] * multiplier_min, ammo_base._ammo_pickup[2] * multiplier_max, math.random())
-- 			picked_up = rng_ammo > 0
			add_amount = math.max(0, math.round(rng_ammo))
		end
		add_amount = math.floor(add_amount * (ratio or 1))
		ammo_base:set_ammo_total(math.clamp(ammo_base:get_ammo_total() + add_amount, 0, ammo_base:get_ammo_max()))
		return picked_up, add_amount
	end
	
	local picked_up, add_amount
	picked_up, add_amount = _add_ammo(self, ratio, add_amount_override)
	for _, gadget in ipairs(self:get_all_override_weapon_gadgets()) do
		if gadget and gadget.ammo_base then
			local p, a = _add_ammo(gadget:ammo_base(), ratio, add_amount_override)
			picked_up = p or picked_up
			add_amount = add_amount + a
		end
	end
	return picked_up, add_amount
end)

