
local function tidy(weapon)
	weapon.AMMO_MAX = weapon.CLIP_AMMO_MAX * weapon.NR_CLIPS_MAX
end

Hook("lib/tweak_data/weapontweakdata", "WeaponTweakData:_init_new_weapons", function(orig, self, ...)
	orig(self, ...)
	
	for name, data in pairs(weapon_tweak_data) do
		local w = self[name]
		-- TODO
	end
	
	-- GL40
	self.gre_m79.reload_speed_multiplier = 2
	self.gre_m79.NR_CLIPS_MAX = 12
	self.gre_m79.AMMO_PICKUP = {-2, 0.65}
	tidy(self.gre_m79)
	
	-- Piglet
	local firerate = 2.5
	self.m32.CLIP_AMMO_MAX = 6
	self.m32.NR_CLIPS_MAX = 4
	self.m32.reload_speed_multiplier = 1.75 -- 1.75x the normal reload speed
	self.m32.single.fire_rate = 1.1 * firerate -- Faster fire rate
	self.m32.fire_mode_data.fire_rate = 1.0 / firerate
	tidy(self.m32)
	
	-- Arbiter
	self.arbiter.reload_speed_multiplier = 0.8 -- slower reloads
	self.arbiter.NR_CLIPS_MAX = 2
	self.arbiter.AMMO_PICKUP = {-2, 0.65}
	tidy(self.arbiter)
	
	-- China Puff
	self.china.NR_CLIPS_MAX = 2
	self.china.AMMO_PICKUP = {-1, 0.65}
	tidy(self.china)
end)

