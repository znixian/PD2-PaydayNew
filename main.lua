
-- setup the weapon tweaks table
weapon_tweak_data = {}

local Location = "" -- TODO do we want to remove this?

-- Load all the tweak files
dofile(Location .. "data/rifles.lua")
dofile(Location .. "data/shotguns.lua")
dofile(Location .. "data/akimbo.lua")
dofile(Location .. "data/snipers.lua")
dofile(Location .. "data/lmgs.lua")
dofile(Location .. "data/specials.lua")
dofile(Location .. "data/pistols.lua")
dofile(Location .. "data/smgs.lua")

dofile(Location .. "data/unaffected.lua")

-- Copy everything into weapon_tweak_data.all
local old_wtd = weapon_tweak_data
local new_wtd = {}

for _, values in pairs(old_wtd) do
	for name, val in pairs(values) do
		new_wtd[name] = val
	end
end

weapon_tweak_data.all = new_wtd


-- Load Everything
dofile(Location .. "weapon_hooks.lua")
dofile(Location .. "unlock_tweak_loader.lua")
dofile(Location .. "properties_tweak_loader.lua")
dofile(Location .. "experience_tweak_loader.lua")

-- This is only to make testing easier.
-- It will enable some things that are very much cheating, but make testing way easier.
dofile(Location .. "devtools.lua")
